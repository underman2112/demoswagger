package com.example.demoswagger;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api")
public class DemoController {

    @GetMapping("/first")
    public String testGet() {
        return "result";
    }

    @PostMapping("/second/{id}")
    public String testPost(@PathVariable String id) {
        String output = "This is output: ";
        return output + id;
    }
}
