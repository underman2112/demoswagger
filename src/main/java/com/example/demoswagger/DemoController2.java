package com.example.demoswagger;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api-demo2")
public class DemoController2 {

    @PostMapping("/first")
    public DemoModel testPut(DemoModel demoModel) {
        return demoModel;
    }

    //    @PostMapping("/first")
    //    public ResponseEntity testPut(DemoModel demoModel){
    //        return ResponseEntity.ok(demoModel);
    //    }
    @DeleteMapping("/second/{id}")
    public String testDelete(@PathVariable String id) {
        String output = "This is output: ";
        return output + id;
    }

    @PatchMapping("/third/{id}")
    public String testPatch(@PathVariable String id) {
        String output = "This is output: ";
        return output + id;
    }
}
