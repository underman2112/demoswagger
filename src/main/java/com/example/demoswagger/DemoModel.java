package com.example.demoswagger;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DemoModel {

    private int id;
    private String name;
    private String numberPhone;
    private String address;
}
